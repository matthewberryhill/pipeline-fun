FROM iron/go:dev

WORKDIR /app

ENV SRC_DIR=/go/src/gitlab.com/matthewberryhill/pipeline-fun
ADD . $SRC_DIR
RUN cd $SRC_DIR; go get
RUN cd $SRC_DIR; go build -o api; cp api /app/

ENTRYPOINT ./api
